import scrapy

class ListingSpider(scrapy.Spider):
    name = "listings"

    start_urls = [
        'https://www.tori.fi/varsinais-suomi/elektroniikka?o=1'
    ]

    custom_settings = {
        'FEED_FORMAT': 'csv',
        'FEED_URI': 'tori.csv',
        'FEED_EXPORT_ENCODING': 'utf-8'
    }

    def parse(self, response):
        for post in response.css('.item_row_flex'):
            yield {
                'title': post.css('.desc_flex .ad-details-left .li-title::text').get(),
                'picture': post.css('.image_container .item_image::attr(src)').get(),
                'date': post.css('.desc_flex .ad-details-right .date-cat-container .date_image::text').get().replace("\n", "").replace("\t", ""),
                'type': post.css('.desc_flex .ad-details-right .date-cat-container .cat_geo p::text')[1].get(),
                'price': post.css('.desc_flex .ad-details-left .list-details-container .list_price::text').get(),
                'link': post.css('a::attr(href)').get()
            }
        next_page = response.xpath("//a[contains(., 'Seuraava sivu')]/@href").get()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)
